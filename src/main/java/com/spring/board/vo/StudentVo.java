package com.spring.board.vo;

public class StudentVo {

	private int student_no;
	private String student_nm;
	private int student_gd;
	private String student_kor;
	private String student_eng;
	private String student_math;
	
	
	@Override
	public String toString() {
		return "StudentVo [student_no=" + student_no + ", student_nm=" + student_nm + ", student_gd=" + student_gd
				+ ", student_kor=" + student_kor + ", student_eng=" + student_eng + ", student_math=" + student_math
				+ "]";
	}
	public int getStudent_no() {
		return student_no;
	}
	public void setStudent_no(int student_no) {
		this.student_no = student_no;
	}
	public String getStudent_nm() {
		return student_nm;
	}
	public void setStudent_nm(String student_nm) {
		this.student_nm = student_nm;
	}
	public int getStudent_gd() {
		return student_gd;
	}
	public void setStudent_gd(int student_gd) {
		this.student_gd = student_gd;
	}
	public String getStudent_kor() {
		return student_kor;
	}
	public void setStudent_kor(String student_kor) {
		this.student_kor = student_kor;
	}
	public String getStudent_eng() {
		return student_eng;
	}
	public void setStudent_eng(String student_eng) {
		this.student_eng = student_eng;
	}
	public String getStudent_math() {
		return student_math;
	}
	public void setStudent_math(String student_math) {
		this.student_math = student_math;
	}
	
}
