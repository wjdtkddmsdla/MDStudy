package com.spring.board.vo;

public class AnimalVo{
	
	private String animal_type;
	private String animal_name;
	private int animal_age;
	
	
	public String getAnimal_type() {
		return animal_type;
	}
	public void setAnimal_type(String animal_type) {
		this.animal_type = animal_type;
	}
	public String getAnimal_name() {
		return animal_name;
	}
	public void setAnimal_name(String animal_name) {
		this.animal_name = animal_name;
	}
	public int getAnimal_age() {
		return animal_age;
	}
	public void setAnimal_age(int animal_age) {
		this.animal_age = animal_age;
	}
	@Override
	public String toString() {
		return "TestVo2 [animal_type=" + animal_type + ", animal_name=" + animal_name + ", animal_age=" + animal_age
				+ "]";
	}

}
