package com.spring.board.service;

import java.util.List;

import com.spring.board.vo.AnimalVo;
import com.spring.board.vo.BoardVo;
import com.spring.board.vo.PageVo;
import com.spring.board.vo.StudentVo;
import com.spring.board.vo.TestVo;

public interface boardService {



	public List<BoardVo> SelectBoardList(PageVo pageVo) throws Exception;

	public BoardVo selectBoard(String boardType, int boardNum) throws Exception;

	public int selectBoardCnt() throws Exception;

	public int boardInsert(BoardVo boardVo) throws Exception;
	
	public List<TestVo> SelectTestList() throws Exception;
	
	public List<AnimalVo> SelectAnimalList() throws Exception;
	
	public List<StudentVo> SelectStudentList() throws Exception;

}
