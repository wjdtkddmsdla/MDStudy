package com.spring.board.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.board.dao.BoardDao;
import com.spring.board.service.boardService;
import com.spring.board.vo.AnimalVo;
import com.spring.board.vo.BoardVo;
import com.spring.board.vo.PageVo;
import com.spring.board.vo.StudentVo;
import com.spring.board.vo.TestVo;

@Service
public class boardServiceImpl implements boardService{
	
	@Autowired
	BoardDao boardDao;
	
	

	public List<StudentVo> selectStudent() throws Exception {
		// TODO Auto-generated method stub
		return boardDao.selectStudent();
	}
	
	public List<AnimalVo> selectAnimal() throws Exception {
		// TODO Auto-generated method stub
		return boardDao.selectAnimal();
	}
	

	public List<BoardVo> SelectBoardList(PageVo pageVo) throws Exception {
		// TODO Auto-generated method stub
		
		return boardDao.selectBoardList(pageVo);
	}
	
	
	public int selectBoardCnt() throws Exception {
		// TODO Auto-generated method stub
		return boardDao.selectBoardCnt();
	}
	
	
	public BoardVo selectBoard(String boardType, int boardNum) throws Exception {
		// TODO Auto-generated method stub
		BoardVo boardVo = new BoardVo();
		
		boardVo.setBoardType(boardType);
		boardVo.setBoardNum(boardNum);
		
		return boardDao.selectBoard(boardVo);
	}
	
	
	public int boardInsert(BoardVo boardVo) throws Exception {
		// TODO Auto-generated method stub
		return boardDao.boardInsert(boardVo);
	}


	public List<TestVo> SelectTestList() throws Exception {
		// TODO Auto-generated method stub
		return boardDao.selectTest();
	}
	
	public List<AnimalVo> SelectAnimalList() throws Exception {
		// TODO Auto-generated method stub
		return boardDao.selectAnimal();
	}
	
	public List<StudentVo> SelectStudentList() throws Exception {
		// TODO Auto-generated method stub
		return boardDao.selectStudent();
	}
	
}
