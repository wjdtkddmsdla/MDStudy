package com.spring.board.dao;

import java.util.List;

import com.spring.board.vo.AnimalVo;
import com.spring.board.vo.BoardVo;
import com.spring.board.vo.PageVo;
import com.spring.board.vo.StudentVo;
import com.spring.board.vo.TestVo;

public interface BoardDao {

	public List<TestVo> selectTest() throws Exception;

	public List<BoardVo> selectBoardList(PageVo pageVo) throws Exception;

	public BoardVo selectBoard(BoardVo boardVo) throws Exception;

	public int selectBoardCnt() throws Exception;

	public int boardInsert(BoardVo boardVo) throws Exception;
	
	public List<AnimalVo> selectAnimal() throws Exception;
	
	public List<StudentVo> selectStudent() throws Exception;

}
