package com.spring.board.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.board.HomeController;
import com.spring.board.service.boardService;
import com.spring.board.vo.AnimalVo;
import com.spring.board.vo.BoardVo;
import com.spring.board.vo.PageVo;
import com.spring.board.vo.StudentVo;
import com.spring.board.vo.TestVo;
import com.spring.common.CommonUtil;

@Controller
public class BoardController {
	
	@Autowired 
	boardService boardService;
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@RequestMapping(value = "/board/boardList.do", method = RequestMethod.GET)
	public String boardList(Locale locale, Model model,PageVo pageVo) throws Exception{
		
		List<BoardVo> boardList = new ArrayList<BoardVo>();
		
		int page = 1;
		int totalCnt = 0;
		
		if(pageVo.getPageNo() == 0){
			pageVo.setPageNo(page);;
		}
		
		boardList = boardService.SelectBoardList(pageVo);
		totalCnt = boardService.selectBoardCnt();
		
		model.addAttribute("boardList", boardList);
		model.addAttribute("totalCnt", totalCnt);
		model.addAttribute("pageNo", page);
		
		return "board/boardList";
	}
	

	@RequestMapping(value = "/test/cat.do", method = RequestMethod.GET)
	public String test() throws Exception{
		
//		List<TestVo> testList = new ArrayList<TestVo>();
//		
//		testList= boardService.SelectTestList(testVo);
//		
//		System.out.println(testList);
//		
		return "test/cat";
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/test/testCat.do", method = RequestMethod.GET)
	public List<TestVo> testView() throws Exception{
		
		List<TestVo> testList = new ArrayList<TestVo>();
		
		testList= boardService.SelectTestList();
		
		System.out.println(testList);
		
		return testList;
	}
	
	@ResponseBody
	@RequestMapping(value = "/test/testAnimal.do", method = RequestMethod.GET)
	public List<AnimalVo> animalView() throws Exception{
		
		List<AnimalVo> animalList = new ArrayList<AnimalVo>();
		
		animalList= boardService.SelectAnimalList();
		
		
		System.out.println(animalList);
		return animalList;
	}
	
	@ResponseBody
	@RequestMapping(value = "/test/testStudent.do", method = RequestMethod.GET)
	public List<StudentVo> StudentView() throws Exception{
		
		List<StudentVo> studentList = new ArrayList<StudentVo>();
		
		studentList= boardService.SelectStudentList();
		
		System.out.println(studentList);
		return studentList;
	}
	
	
	@RequestMapping(value = "/board/{boardType}/{boardNum}/boardView.do", method = RequestMethod.GET)
	public String boardView(Locale locale, Model model
			,@PathVariable("boardType")String boardType
			,@PathVariable("boardNum")int boardNum) throws Exception{
		
		BoardVo boardVo = new BoardVo();
		
		
		boardVo = boardService.selectBoard(boardType,boardNum);
		
		model.addAttribute("boardType", boardType);
		model.addAttribute("boardNum", boardNum);
		model.addAttribute("board", boardVo);
		
		return "board/boardView";
	}

	
	@RequestMapping(value = "/board/boardWrite.do", method = RequestMethod.GET)
	public String boardWrite(Locale locale, Model model) throws Exception{
		
		
		return "board/boardWrite";
	}
	
	@RequestMapping(value = "/board/boardWriteAction.do", method = RequestMethod.POST)
	@ResponseBody
	public String boardWriteAction(Locale locale,BoardVo boardVo) throws Exception{
		
		HashMap<String, String> result = new HashMap<String, String>();
		CommonUtil commonUtil = new CommonUtil();
		
		int resultCnt = boardService.boardInsert(boardVo);
		
		result.put("success", (resultCnt > 0)?"Y":"N");
		String callbackMsg = commonUtil.getJsonCallBackString(" ",result);
		
		System.out.println("callbackMsg::"+callbackMsg);
		
		return callbackMsg;
	}
	
}
