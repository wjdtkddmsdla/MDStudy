var cnt = 0;
// var tbArr = new Map();
function table(engArr,korArr,divName,data ,selectArr) {
    
    if(engArr.length != korArr.length){
        alert("영문명 개수와 한글명 개수가 일치하지 않습니다.");
        return;
    }   
    var tableNm = 'table' + cnt;
    var table = document.createElement('table');
    table.id = tableNm;
    table.className = "table table-hover"
    // table.style.border = '1px solid black'
    document.getElementById(divName).appendChild(table);
    var tbody = document.createElement('tbody');
    tbody.id = 'tbody' + cnt;
    document.getElementById('table'+cnt).appendChild(tbody);
    var tr = document.createElement('tr');
    tr.id = 'tr' + cnt + '_0';
    tr.className = 'table-primary'
    document.getElementById('tbody'+cnt).appendChild(tr);


    for(var i = 0; i < engArr.length ; i++){
        console.log(i + "번")

        var th = document.createElement('th');
        th.id = engArr[i] ;
		th.style.width = '50px';
		// th.style.border = "1px solid black";
        // th.style.backgroundColor = "lightgray";
        th.innerText = korArr[i] ;
        
        document.getElementById('tr'+cnt+'_0').appendChild(th);
        
    }
    // tbArr.set(tableNm,cnt)
    selectList(data,engArr);
    if(selectArr.length != 0){

        sumAdd(tableNm,engArr,selectArr)

    }

    cnt++;
}

function selectList(data , engArr){
   
  
    
    
    for(var i = 0; i < data.length ; i++){
	
  		var datatr = document.createElement('tr');
        datatr.id = 'tr'+ cnt + '_' + ( i + 1 );
		// datatr.style.border = '1px solid black';
        datatr.className = "table-light";
        document.getElementById('tbody'+cnt).appendChild(datatr);

        for(var j = 0 ; j < engArr.length ; j ++ ){

            var td = document.createElement('td');
            td.id = engArr[j] + i + '_'+ ( i + 1 );
			// td.style.border = "1px solid black";
            document.getElementById('tr'+ cnt + '_' + ( i + 1 )).appendChild(td);
            var input = document.createElement('input');
            input.id = 'id' + engArr[j] +'_'+ ( i + 1 ) ;
            input.name = 'name' + engArr[j]+'_' + ( i + 1 );
            input.className = "form-control-plaintext";
            input.readOnly = true;
			// input.style.width = '50px'
            var colName = engArr[j];
			var inputData = data[i][colName]; 
            input.value = inputData;
            document.getElementById(engArr[j] + i + '_' + ( i + 1 )).appendChild(input);
        }

    }
    
}

function sumAdd(tableName , engArr , selectArr) {

    var rowCnt = document.getElementById(tableName).rows.length;
    var tr = document.createElement('tr');
    tr.id = 'tr' + cnt + '_footer';
    tr.className = "table-info";
    document.getElementById('tbody'+ cnt).appendChild(tr);
    
    for(var i = 0 ; i < engArr.length ; i++){
        
        var sum = 0;
        var td = document.createElement('td');
        td.id = engArr[i]+ '_footer';
    
        // td.style.border = '1px solid black';
        // td.style.backgroundColor = 'yellow';
        for(var j = 0 ; j < selectArr.length ; j++){
           
            if(i == selectArr[j]){
                
                for(var z = 0 ; z < (rowCnt-1) ; z++){

                   sum = sum + parseInt(document.getElementById('id' + engArr[i] + '_' + (z+1)).value);

                }
            }

        }
        if(sum != 0){
            td.innerText = sum;
            }
        document.getElementById('tr' + cnt + '_footer').appendChild(td);
       
    }
   

}